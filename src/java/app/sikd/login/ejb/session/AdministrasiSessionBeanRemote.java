/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.session;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author sora
 */
@Remote
public interface AdministrasiSessionBeanRemote {

    public List<TUserGroup> getSubUserGroups(TUserGroup parentGroup) throws Exception;

    public TUserGroup getUserGroup(long idGroup) throws Exception;

    public TUserGroup createUserGroup(TUserGroup grup) throws Exception;

    public void deleteUserGroup(TUserGroup grup) throws Exception;

    public void updateUserGroup(TUserGroup grup) throws Exception;

    public WilayahKerja getWilayahKerja(long idWK) throws Exception;

    public List<WilayahKerja> getWilayahKerjas() throws Exception;

    public WilayahKerja createWilayahKerja(WilayahKerja obj) throws Exception;

    public void deleteWilayahKerja(WilayahKerja obj) throws Exception;

    public void updateWilayahKerja(WilayahKerja obj) throws Exception;

    public Pemda getPemda(long idPemda) throws Exception;

    public List<Pemda> getPemdas() throws Exception;

    public Pemda createPemda(Pemda obj) throws Exception;

    public void deletePemda(Pemda obj) throws Exception;

    public void updatePemda(Pemda obj) throws Exception;
    public List<Pemda> getProvinsis() throws Exception;
    public List<Pemda> getPemdaByProvinsi(Pemda prov) throws Exception;
    
    public List<TGroupRight> getGroupRight(long idGrup) throws Exception;
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects) throws Exception;
    public void deleteGroupRightByGroup(long groupId) throws Exception;
    public void updateGroupRight(long groupId, List<TGroupRight> objects) throws Exception;
    
}
