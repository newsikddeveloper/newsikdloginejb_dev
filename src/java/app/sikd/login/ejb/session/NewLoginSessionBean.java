package app.sikd.login.ejb.session;

import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.bl.ILoginBusinessLogic;
import app.sikd.login.ejb.bl.LoginBusinessLogic;
import app.sikd.login.ejb.db.LoginConnectionManager;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({NewLoginSessionBeanRemote.class})
public class NewLoginSessionBean implements NewLoginSessionBeanRemote {
    Connection loginConn;
    Connection sikdConn;
    ILoginBusinessLogic bl;
    String uploadPath;
    private String menuDoc;
    
    public NewLoginSessionBean(){
        bl=new LoginBusinessLogic();
    }
    
    Connection buatLoginConnection(){ 
        try {
//            System.out.println("Mulai membuat login Connection ke database login");
            LoginConnectionManager cm = new LoginConnectionManager();
            loginConn = cm.createConnection("loginsikd.properties");
//            System.out.println("Sukses buat login Connection ke database " + loginConn );
            uploadPath = cm.getUploadPath();
            return loginConn;
        } catch (Exception ex) {
            Logger.getLogger(NewLoginSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    Connection buatSIKDConnection(){
        try {
            LoginConnectionManager cm = new LoginConnectionManager();            
            sikdConn = cm.createConnection("sikd.properties");
            return sikdConn;
        } catch (Exception ex) {
            Logger.getLogger(NewLoginSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    @Override
    public TUserAccount login(String userName, byte[] pass) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        TUserAccount userAccount = bl.login(userName, pass, loginConn); 
        loginConn.close();        
        
        return userAccount;
    }
    public TUserAccount getUserbyName(String userName) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        TUserAccount userAccount = bl.getUserbyName(userName, loginConn); 
        loginConn.close();        
        
        return userAccount;
    }
    public boolean canRead(long idgrup, String menuName) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        boolean result = bl.canRead(idgrup, menuName, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public boolean canWrite(long idgrup, String menuName) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        boolean result = bl.canWrite(idgrup, menuName, loginConn); 
        loginConn.close();        
        
        return result;
    }
    
    public String[] getKodeSatkerKodePemda(String userName, String password) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        String[] result = bl.getKodeSatkerKodePemda(userName, password, loginConn); 
        loginConn.close();        
        
        return result;
    }

    public String getMenuDoc() {
        return menuDoc;
    }

    public void setMenuDoc(String menuDoc) {
        this.menuDoc = menuDoc;
    }
    
    
}
