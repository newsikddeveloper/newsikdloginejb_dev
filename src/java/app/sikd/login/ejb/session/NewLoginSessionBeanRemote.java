/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.session;

import app.sikd.entity.mgr.TUserAccount;
import java.sql.Connection;
import javax.ejb.Remote;

/**
 *
 * @author detra
 */
@Remote
public interface NewLoginSessionBeanRemote {

    public TUserAccount login(String userName, byte[] pass) throws Exception;

    public boolean canWrite(long idgrup, String menuName) throws Exception;

    public boolean canRead(long idgrup, String menuName) throws Exception;

    public TUserAccount getUserbyName(String userName) throws Exception;

    public String[] getKodeSatkerKodePemda(String userName, String password) throws Exception;

    public String getMenuDoc();

    public void setMenuDoc(String menuDoc);
    
}
