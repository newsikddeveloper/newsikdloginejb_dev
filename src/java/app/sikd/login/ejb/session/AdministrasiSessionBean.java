/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.session;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.bl.AdministrasiBL;
import app.sikd.login.ejb.bl.IAdministrasiBL;
import app.sikd.login.ejb.db.LoginConnectionManager;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author detra
 */
@Stateless
@Remote({AdministrasiSessionBeanRemote.class})
public class AdministrasiSessionBean implements AdministrasiSessionBeanRemote {
        Connection loginConn;
    IAdministrasiBL bl;
    
    public AdministrasiSessionBean(){
        bl= new AdministrasiBL();
    }
    
    Connection buatLoginConnection(){ 
        try {
//            System.out.println("Mulai membuat login Connection ke database login");
            LoginConnectionManager cm = new LoginConnectionManager();
            loginConn = cm.createConnection("loginsikd.properties");            
            System.out.println("Sukses buat login Connection ke database " + loginConn );
            return loginConn;
        } catch (Exception ex) {
            Logger.getLogger(AdministrasiSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<TUserGroup> getSubUserGroups(TUserGroup parentGroup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<TUserGroup> result = bl.getSubUserGroups(parentGroup, loginConn); 
        loginConn.close();        
        
        return result;
    }
    
    public TUserGroup getUserGroup(long idGroup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        TUserGroup result = bl.getUserGroup(idGroup, loginConn); 
        loginConn.close();        
        
        return result;
    }
    
    public TUserGroup createUserGroup(TUserGroup grup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        TUserGroup result = bl.createUserGroup(grup, loginConn); 
        loginConn.close();        
        
        return result;
    }
    
    public void deleteUserGroup(TUserGroup grup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.deleteUserGroup(grup, loginConn); 
        loginConn.close();        
    }
    
    
    public void updateUserGroup(TUserGroup grup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.updateUserGroup(grup, loginConn); 
        loginConn.close();
    }
    
    
    public WilayahKerja getWilayahKerja(long idWK) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        WilayahKerja result = bl.getWilayahKerja(idWK, loginConn); 
        loginConn.close();        
        
        return result;
    }
    

    public  List<WilayahKerja> getWilayahKerjas() throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<WilayahKerja> result = bl.getWilayahKerjas(loginConn); 
        loginConn.close();        
        
        return result;
    }
    
    public WilayahKerja createWilayahKerja(WilayahKerja obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        WilayahKerja result = bl.createWilayahKerja(obj, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public void deleteWilayahKerja(WilayahKerja obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.deleteWilayahKerja( obj, loginConn); 
        loginConn.close();        
    }
    public void updateWilayahKerja(WilayahKerja obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.updateWilayahKerja(obj, loginConn); 
        loginConn.close();
    }
    
    public Pemda getPemda(long idPemda) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        Pemda result = bl.getPemda(idPemda, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public  List<Pemda> getPemdas() throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<Pemda> result = bl.getPemdas(loginConn); 
        loginConn.close();        
        
        return result;
    }
    public List<Pemda> getProvinsis() throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<Pemda> result = bl.getProvinsis(loginConn); 
        loginConn.close();        
        
        return result;
    }
    public List<Pemda> getPemdaByProvinsi(Pemda prov) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<Pemda> result = bl.getPemdaByProvinsi(prov, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public Pemda createPemda(Pemda obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        Pemda result = bl.createPemda(obj, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public void deletePemda(Pemda obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.deletePemda(obj, loginConn); 
        loginConn.close();        
    }
    public void updatePemda(Pemda obj) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.updatePemda(obj, loginConn); 
        loginConn.close();        
    }
    
    public List<TGroupRight> getGroupRight(long idGrup) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<TGroupRight> result = bl.getGroupRight(idGrup, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        List<TGroupRight> result = bl.createGroupRights(groupId, objects, loginConn); 
        loginConn.close();        
        
        return result;
    }
    public void deleteGroupRightByGroup(long groupId) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.deleteGroupRightByGroup(groupId, loginConn); 
        loginConn.close();        
    }
    public void updateGroupRight(long groupId, List<TGroupRight> objects) throws Exception{
        if( loginConn == null || loginConn.isClosed() ) loginConn = buatLoginConnection();
        bl.updateGroupRight(groupId, objects, loginConn); 
        loginConn.close();        
    }
    
}
