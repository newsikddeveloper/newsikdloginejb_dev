package app.sikd.login.ejb.bl;

import app.sikd.entity.mgr.TUserAccount;
import app.sikd.login.ejb.db.ILoginSQL;
import app.sikd.login.ejb.db.LoginSQL;
import app.sikd.util.Crypto;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author detra
 */
public class LoginBusinessLogic implements ILoginBusinessLogic{
    ILoginSQL sql;

    public LoginBusinessLogic(){
        sql = new LoginSQL();
    }
    
    public TUserAccount login(String userName, byte[] pass, Connection loginConn) throws Exception{
        try {            
            byte[] epass = Crypto.encrypt(pass);
            return sql.login(userName, epass, loginConn);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | SQLException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
            throw new Exception("LoginBusinessLogic: "+ ex.getMessage());
        }
    }
    
    public String[] getKodeSatkerKodePemda(String userName, String password, Connection loginConn) throws Exception{
        try {                        
            return sql.getKodeSatkerKodePemda(userName, password, loginConn);
        } catch (SQLException ex) {
            throw new Exception("LoginBusinessLogic: "+ ex.getMessage());
        }
    }
            
    public TUserAccount getUserbyName(String userName, Connection loginConn) throws Exception{
        try {            
            return sql.getUserbyName(userName, loginConn);
        } catch (SQLException ex) {
            throw new Exception("LoginBusinessLogic: "+ ex.getMessage());
        }
    }
    public boolean canRead(long idGrup, String menuName, Connection loginConn) throws Exception{
        try {            
            return sql.canRead(idGrup, menuName, loginConn);
        } catch (SQLException ex) {
            throw new Exception("LoginBusinessLogic: "+ ex.getMessage());
        }
    }
    public boolean canWrite(long idGrup, String menuName, Connection loginConn) throws Exception{
        try {            
            return sql.canWrite(idGrup, menuName, loginConn);
        } catch (SQLException ex) {
            throw new Exception("LoginBusinessLogic: "+ ex.getMessage());
        }
    }

}
