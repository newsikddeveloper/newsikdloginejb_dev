package app.sikd.login.ejb.bl;

import app.sikd.entity.mgr.TUserAccount;
import java.sql.Connection;

/**
 *
 * @author detra
 */
public interface ILoginBusinessLogic {
    public TUserAccount login(String userName, byte[] pass, Connection loginConn) throws Exception;
    public TUserAccount getUserbyName(String userName, Connection loginConn) throws Exception;
    public boolean canRead(long idGrup, String menuName, Connection loginConn) throws Exception;
    public boolean canWrite(long idGrup, String menuName, Connection loginConn) throws Exception;
    
    public String[] getKodeSatkerKodePemda(String userName, String password, Connection loginConn) throws Exception;

}
