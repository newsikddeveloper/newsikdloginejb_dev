
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.bl;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.login.ejb.db.AdministrasiPenggunaSQL;
import app.sikd.login.ejb.db.IAdministrasiPengguna;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public class AdministrasiBL implements IAdministrasiBL{
    
    IAdministrasiPengguna sql;

    public AdministrasiBL(){
        sql = new AdministrasiPenggunaSQL();
    }
    public List<TUserGroup> getSubUserGroups(TUserGroup parentGroup, Connection loginConn) throws Exception{
        try {
            return sql.getSubUserGroups(parentGroup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public TUserGroup getUserGroup(long idGroup, Connection loginConn) throws Exception{
        try {
            return sql.getUserGroup(idGroup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public TUserGroup createUserGroup(TUserGroup grup, Connection loginConn) throws Exception{
        try {
            return sql.createUserGroup(grup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public void deleteUserGroup(TUserGroup grup, Connection loginConn) throws Exception{
        try {
            sql.deleteUserGroup(grup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public void updateUserGroup(TUserGroup grup, Connection loginConn) throws Exception{
        try {
            sql.updateUserGroup(grup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    
    public WilayahKerja getWilayahKerja(long idWK, Connection loginConn) throws Exception{
        try {
            return sql.getWilayahKerja(idWK, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public  List<WilayahKerja> getWilayahKerjas(Connection loginConn) throws Exception{
        try {
            return sql.getWilayahKerjas(loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public WilayahKerja createWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception{
        try {
            return sql.createWilayahKerja(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public void deleteWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception{
        try {
            sql.deleteWilayahKerja(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public void updateWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception{
        try {
            sql.updateWilayahKerja(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public Pemda getPemda(long idPemda, Connection loginConn) throws Exception{
        try {
            return sql.getPemda(idPemda, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public  List<Pemda> getPemdas(Connection loginConn) throws Exception{
        try {
            return sql.getPemdas(loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public List<Pemda> getProvinsis(Connection loginConn) throws Exception{
        try {
            return sql.getProvinsis(loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public List<Pemda> getPemdaByProvinsi(Pemda prov, Connection loginConn) throws Exception{
        try {
            return sql.getPemdaByProvinsi(prov, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public Pemda createPemda(Pemda obj, Connection loginConn) throws Exception{
        try {
            return sql.createPemda(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public void deletePemda(Pemda obj, Connection loginConn) throws Exception{
        try {
            sql.deletePemda(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public void updatePemda(Pemda obj, Connection loginConn) throws Exception{
        try {
            sql.updatePemda(obj, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
    public List<TGroupRight> getGroupRight(long idGrup, Connection loginConn) throws Exception{
        try {
            return sql.getGroupRight(idGrup, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects, Connection loginConn) throws Exception{
        try {
            return sql.createGroupRights(groupId, objects, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public void deleteGroupRightByGroup(long groupId, Connection loginConn) throws Exception{
        try {
            sql.deleteGroupRightByGroup(groupId, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    public void updateGroupRight(long groupId, List<TGroupRight> objects, Connection loginConn) throws Exception{
        try {
            sql.updateGroupRight(groupId, objects, loginConn);
        } catch (SQLException ex) {
            throw new Exception("AdministrasiBL: "+ ex.getMessage());
        }
    }
    
}
