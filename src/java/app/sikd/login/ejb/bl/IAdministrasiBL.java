/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.bl;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IAdministrasiBL {
    public List<TUserGroup> getSubUserGroups(TUserGroup parentGroup, Connection loginConn) throws Exception;
    public TUserGroup getUserGroup(long idGroup, Connection loginConn) throws Exception;
    public TUserGroup createUserGroup(TUserGroup grup, Connection loginConn) throws Exception;
    public void deleteUserGroup(TUserGroup grup, Connection loginConn) throws Exception;
    public void updateUserGroup(TUserGroup grup, Connection loginConn) throws Exception;
    
    public WilayahKerja getWilayahKerja(long idWK, Connection loginConn) throws Exception;
    public  List<WilayahKerja> getWilayahKerjas(Connection loginConn) throws Exception;
    public WilayahKerja createWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception;
    public void deleteWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception;
    public void updateWilayahKerja(WilayahKerja obj, Connection loginConn) throws Exception;
    
    public Pemda getPemda(long idPemda, Connection loginConn) throws Exception;
    public  List<Pemda> getPemdas(Connection loginConn) throws Exception;
    public Pemda createPemda(Pemda obj, Connection loginConn) throws Exception;
    public void deletePemda(Pemda obj, Connection loginConn) throws Exception;
    public void updatePemda(Pemda obj, Connection loginConn) throws Exception;
    public List<Pemda> getProvinsis(Connection loginConn) throws Exception;
    public List<Pemda> getPemdaByProvinsi(Pemda prov, Connection loginConn) throws Exception;
    
    public List<TGroupRight> getGroupRight(long idGrup, Connection loginConn) throws Exception;
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects, Connection loginConn) throws Exception;
    public void deleteGroupRightByGroup(long groupId, Connection loginConn) throws Exception;
    public void updateGroupRight(long groupId, List<TGroupRight> objects, Connection loginConn) throws Exception;
    
}
