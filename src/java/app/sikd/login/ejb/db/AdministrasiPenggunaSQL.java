/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.db;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.MasterMenu;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sora
 */
public class AdministrasiPenggunaSQL implements IAdministrasiPengguna{
    
    public TUserGroup getUserGroup(long idGroup, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT masterkey, groupname, description\n"
                    + "  FROM tusergroup where masterkey = " + idGroup);
            TUserGroup result = null;
            if( rs.next()){
                result = new TUserGroup(rs.getLong(1), rs.getString(2), rs.getString(3));                
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil UserGroup \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public  List<TUserGroup> getSubUserGroups(TUserGroup parentGroup, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT masterkey, groupname, description "
                    + "  FROM tusergroup where masterkey in (select subgroup from tusergroupstruktur where supergroup=" + parentGroup.getId() + ") ");
            List<TUserGroup> result = new ArrayList();
            while( rs.next()){
                result.add(new TUserGroup(rs.getLong(1), rs.getString(2), rs.getString(3), parentGroup));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil SubUserGroup \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public TUserGroup createUserGroup(TUserGroup grup, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("with zz as ("
                    + " INSERT INTO tusergroup (groupname, description) "
                    + " VALUES ('"+grup.getNama() + "', '" + grup.getDeskripsi() + "') returning *)"
                    + " select max(masterkey) id from zz");
            if( rs.next()){
                long id = rs.getLong(1);
                grup.setId(id);                
            }
            if( grup.getParentGroup() != null && grup.getParentGroup().getId() > 0) {
                stm.executeUpdate("insert into tusergroupstruktur "
                        + " VALUES(" + grup.getParentGroup().getId() + ", "+grup.getId()+ ")");
            }
            return grup;
        } catch (SQLException ex) {
            throw new SQLException("Gagal membuat UserGroup \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public void deleteUserGroup(TUserGroup grup, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " DELETE FROM tusergroup where masterkey = " + grup.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus UserGroup \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public void updateUserGroup(TUserGroup grup, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " UPDATE tusergroup "
                    + " SET groupname= '" + grup.getNama().trim() + "', description= '" + grup.getDeskripsi() + "' "
                    + " where masterkey = " + grup.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal merubah UserGroup \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public List<TGroupRight> getGroupRight(long idGrup, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT recordindex, masterkey mmkey , nama, address, actives, reads, writes "
                    + "  FROM tmastermenu "
                    + "  left join tgroupright on tmastermenu.masterkey=tgroupright.menu "
                    + "  where tgroupright.usergroup= " + idGrup + " and actives = true "
                    + " order by mmkey");
            List<TGroupRight> result = new ArrayList();
            while( rs.next()){
                result.add(new TGroupRight(rs.getLong("recordindex"), new MasterMenu(rs.getLong("mmkey"), rs.getString("nama"), rs.getString("address"), rs.getBoolean("actives")), rs.getBoolean("reads"), rs.getBoolean("writes")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil GroupRight \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    private TGroupRight getGroupRight(long idGrup, long menu, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT recordindex, masterkey mmkey , nama, address, actives, reads, writes "
                    + "  FROM tmastermenu "
                    + "  left join tgroupright on tmastermenu.masterkey=tgroupright.menu "
                    + "  where tgroupright.usergroup= " + idGrup + " and menu = "+menu);
            TGroupRight result = null;
            if( rs.next()){
                result = new TGroupRight(rs.getLong("recordindex"), new MasterMenu(rs.getLong("mmkey"), rs.getString("nama"), rs.getString("address"), rs.getBoolean("actives")), rs.getBoolean("reads"), rs.getBoolean("writes"));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil GroupRight \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects, Connection loginConn) throws SQLException{
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "with zz as("
                + "  INSERT INTO tgroupright( usergroup, menu, reads, writes) "
                + "    VALUES (?, ?, ?, ?) returning * ) select max(recordindex) id from zz";
        try {
            stm = loginConn.prepareStatement(sql);
            if( objects != null ){
                for (int i = 0; i < objects.size(); i++) {
                    TGroupRight get = objects.get(i);
                    int col=0;
                    stm.setLong(++col, groupId);
                    stm.setLong(++col, get.getMenu().getId());
                    stm.setBoolean(++col, get.isReads());
                    stm.setBoolean(++col, get.isWrites());
                    rs = stm.executeQuery();
                    if( rs.next() ){
                        objects.get(i).setId(rs.getLong("id"));
                    }
                }
            }
            return objects;
        } catch (SQLException ex) {
            throw new SQLException("Gagal membuat GroupRight\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    private TGroupRight createGroupRight(long groupId, TGroupRight object, Connection loginConn) throws SQLException{
        PreparedStatement stm = null;
        ResultSet rs = null;
        String sql = "with zz as("
                + "  INSERT INTO tgroupright( usergroup, menu, reads, writes) "
                + "    VALUES (?, ?, ?, ?) returning * ) select max(recordindex) id from zz";
        try {
            stm = loginConn.prepareStatement(sql);
                    int col=0;
                    stm.setLong(++col, groupId);
                    stm.setLong(++col, object.getMenu().getId());
                    stm.setBoolean(++col, object.isReads());
                    stm.setBoolean(++col, object.isWrites());
                    rs = stm.executeQuery();
                    if( rs.next() ){
                        object.setId(rs.getLong("id"));
                    }
            return object;
        } catch (SQLException ex) {
            throw new SQLException("Gagal membuat GroupRight\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public void deleteGroupRightByGroup(long groupId, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " DELETE FROM tgroupright where usergroup = " + groupId;
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus GroupRight \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    private void deleteGroupRight(long groupId, long menuId, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " DELETE FROM tgroupright where usergroup = " + groupId + " and menu="+menuId;
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus GroupRight \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public void updateGroupRight(long groupId, List<TGroupRight> objects, Connection loginConn) throws SQLException{
        PreparedStatement stm = null;
        String sql = " UPDATE tgroupright "
                + " SET reads=?, writes=? "
                + "  where usergroup=? and menu =?";
        try {
            stm = loginConn.prepareStatement(sql);
            if( objects != null ){
                for (int i = 0; i < objects.size(); i++) {                    
                    TGroupRight get = objects.get(i);
                    if (get.isReads() || get.isWrites()) {
                        TGroupRight tgr = getGroupRight(groupId, get.getMenu().getId(), loginConn);
                        if (tgr != null && tgr.getId() > 0) {
                            stm.setBoolean(1, get.isReads());
                            stm.setBoolean(2, get.isWrites());
                            stm.setLong(3, groupId);
                            stm.setLong(4, get.getMenu().getId());
                            stm.executeUpdate();
                        }
                        else{
                            objects.set(i, createGroupRight(groupId, get, loginConn));
                        }
                    }
                    else{
                        deleteGroupRight(groupId, get.getMenu().getId(), loginConn);
                    }
                    
                }
            }
        } catch (SQLException ex) {
            throw new SQLException("Gagal merubah GroupRight \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public WilayahKerja getWilayahKerja(long idWK, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            
            rs = stm.executeQuery("SELECT * FROM twilkerja "
                    + "where idwilkerja = " + idWK);
            WilayahKerja result = null;
            if( rs.next()){
                result = new WilayahKerja(rs.getLong(1), rs.getString(2), rs.getString(3));                
                result.setPemdas(getPemdasByWilayahKerja(result.getId(), loginConn));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil WilayahKerja \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public  List<WilayahKerja> getWilayahKerjas(Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {            
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT * FROM twilkerja");
            List<WilayahKerja> result = new ArrayList();
            while( rs.next()){
                WilayahKerja w = new WilayahKerja(rs.getLong(1), rs.getString(2), rs.getString(3));
                w.setPemdas(getPemdasByWilayahKerja(w.getId(), loginConn));
                result.add(w);
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil WilayahKerja \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public WilayahKerja createWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            
            rs = stm.executeQuery("with zz as ("
                    + " INSERT INTO twilkerja (nama, description) "
                    + " VALUES ('"+obj.getNama() + "', '" + obj.getDeskripsi() + "') returning *)"
                    + " select max(idwilkerja) id from zz");
            if( rs.next()){
                long id = rs.getLong(1);
                obj.setId(id);
            }
            if (obj.getId() > 0) {
                if (obj.getPemdas() != null && !obj.getPemdas().isEmpty()) {
                    for (int i = 0; i < obj.getPemdas().size(); i++) {
                        stm.executeUpdate("INSERT INTO twilkerja_has_tpemda(idwilkerja, idpemda) "
                                + " VALUES (" + obj.getId() + ", " + obj.getPemdas().get(i).getId() + ")");

                    }
                }
            }
            return obj;
        } catch (SQLException ex) {
            throw new SQLException("Gagal membuat WilayahKerja \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public void deleteWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " DELETE FROM twilkerja where idwilkerja = " + obj.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus data Wilayah Kerja \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public void updateWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " UPDATE twilkerja "
                    + " SET nama= '" + obj.getNama().trim() + "', description= '" + obj.getDeskripsi() + "' "
                    + " where idwilkerja = " + obj.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
            
            stm.executeUpdate("delete from twilkerja_has_tpemda where idwilkerja="+obj.getId());
            if( obj.getPemdas()!=null&& !obj.getPemdas().isEmpty()){
                for (int i = 0; i < obj.getPemdas().size(); i++) {
                    stm.executeUpdate("INSERT INTO twilkerja_has_tpemda(idwilkerja, idpemda) "
                            + " VALUES (" + obj.getId() + ", "+obj.getPemdas().get(i).getId()+")");
                }
            }
        } catch (SQLException ex) {
            throw new SQLException("Gagal merubah WilayahKerja \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public Pemda getPemda(long idPemda, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            
            rs = stm.executeQuery("SELECT * FROM tpemda "
                    + " where idpemda = " + idPemda);
            Pemda result = null;
            if( rs.next()){
                result = new Pemda(rs.getLong(1), rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getShort(7));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public List<Pemda> getProvinsis(Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            
            rs = stm.executeQuery("SELECT * FROM tpemda "
                    + " where kodepemda = '00' and tipepemda=0");
            List<Pemda> result = new ArrayList();
            while( rs.next()){
                result.add(new Pemda(rs.getLong(1), rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getShort(7)));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public  List<Pemda> getPemdas(Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {            
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT * FROM tpemda");
            List<Pemda> result = new ArrayList();
            
            while( rs.next()){
                result.add(new Pemda(rs.getLong(1), rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getShort(7)));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public  List<Pemda> getPemdasByWilayahKerja(long idWilKerja, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {            
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT tp.* "
                    + "  FROM tpemda tp "
                    + "  inner join twilkerja_has_tpemda twp on tp.idpemda =  twp.idpemda where twp.idwilkerja= "+idWilKerja
                    + " order by kodeprovinsi, kodepemda");
            List<Pemda> result = new ArrayList();
            
            while( rs.next()){
                result.add(new Pemda(rs.getLong(1), rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getShort(7)));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public Pemda createPemda(Pemda obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            
            rs = stm.executeQuery("with zz as ("
                    + " INSERT INTO tpemda "
                    + " (kodeprovinsi, kodepemda, kodesatker, namapemda, namapemdasingkat, tipepemda) "
                    + " VALUES ('"+obj.getKodeProvinsi()+ "', '" + obj.getKodePemda() + "', '" 
                    + obj.getKodeSatker() + "', '" + obj.getNamaPemda() + "', '" 
                    + obj.getNamaSingkatPemda() + "', " + obj.getTipePemda()
                    + ") returning *)"
                    + " select max(idpemda) id from zz");
            if( rs.next()){
                long id = rs.getLong(1);
                obj.setId(id);                
            }
            return obj;
        } catch (SQLException ex) {
            throw new SQLException("Gagal membuat Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public void deletePemda(Pemda obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " DELETE FROM tpemda where idpemda = " + obj.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal Menghapus Data Pemda \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public void updatePemda(Pemda obj, Connection loginConn) throws SQLException{
        Statement stm = null;
        String sql = " UPDATE tpemda "
                + " set " 
                + " kodeprovinsi= '" + obj.getKodeProvinsi() + "', "
                + " kodepemda= '" + obj.getKodePemda()+ "', "
                + " kodesatker = '" + obj.getKodeSatker()+ "', "
                + " namapemda= '" + obj.getNamaPemda()+ "', "
                + " namapemdasingkat= '"  + obj.getNamaSingkatPemda()+ "', "
                + " tipepemda="  + obj.getTipePemda()
                + " where idpemda = " + obj.getId();
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new SQLException("Gagal merubah Data Pemda \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    public List<Pemda> getPemdaByProvinsi(Pemda prov, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        String sql = " select * from tpemda "
                + " where kodeprovinsi= '" + prov.getKodeProvinsi() + "' ";
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery(sql);
            List<Pemda> result = new ArrayList();
            
            while( rs.next()){
                result.add(new Pemda(rs.getLong(1), rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getShort(7)));
                
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil Data Pemda \n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    
    
    
    
}
