package app.sikd.login.ejb.db;

import app.sikd.entity.Pemda;
import app.sikd.entity.mgr.IMGRDBConstants;
import app.sikd.entity.mgr.MasterMenu;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TMenu;
import app.sikd.entity.mgr.TUserAccount;
import app.sikd.entity.mgr.TUserContact;
import app.sikd.entity.mgr.TUserGroup;
import app.sikd.util.Crypto;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author detra
 */
public class LoginSQL implements ILoginSQL{
    
    @Override
    public TUserAccount login(String userName, byte[] encryptedPass, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select ua.*, uc." + IMGRDBConstants.ATTR_FULL_NAME + " from tuseraccount ua "
                    + " inner join tusercontact uc "
                    + " on ua.recordindex = uc.useraccount "
                    + " where ua.username = '" + userName + "'");
            byte[] pass;
            TUserAccount result = null;
            if( rs.next() ){
                result = new TUserAccount(rs.getLong("recordindex"), userName, encryptedPass, rs.getBoolean("isactive"));
                result.setGroup(new TUserGroup(rs.getLong("idgroup")));
                pass = rs.getBytes("password");
                result.setPassword(Crypto.decrypt(pass));
                if (!Arrays.equals(pass, encryptedPass)) {
                    throw new SQLException("Gagal melakukan login pada aplikasi ini\n Kata Sandi tidak Sesuai");
                }
                if( !rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE) ){
                    throw new SQLException("Gagal melakukan login pada aplikasi ini\n Akun anda tidak aktif, hubungi admin untuk mengaktifkan kembali akun anda");
                }
                TUserContact uc = new TUserContact();
                uc.setFullName(rs.getString("fullname"));
                result.setUserContact(uc);
            }
            else{
                throw new SQLException("Gagal melakukan login pada aplikasi ini\nUser Name tidak ditemukan");
            }
                
                rs = stm.executeQuery("select tp.* from TPemda tp where idpemda in "
                        + " (select idpemda from tuserpemda where iduser=" + result.getId() + ")");
                Pemda pemda = null;
                if(rs.next()){
                    pemda= new Pemda(rs.getLong("idpemda"), rs.getString("kodeprovinsi"), 
                            rs.getString("kodepemda"), rs.getString("kodesatker"), 
                            rs.getString("namapemda"), rs.getString("namapemdasingkat"), rs.getShort("tipepemda"));
                }
                result.setPemda(pemda);
                result.setMenus(getUserMenuStructures(result.getId(), loginConn));
            result.getGroup().setRights(getUserGroupMenuRight(result.getGroup().getId(), loginConn));
            return result;
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | SQLException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
            throw new SQLException("Gagal melakukan login pada aplikasi ini\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
       
    public TUserAccount getUserbyName(String userName, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select ua.*, uc." + IMGRDBConstants.ATTR_FULL_NAME + " from tuseraccount ua "
                    + " inner join tusercontact uc "
                    + " on ua.recordindex = uc.useraccount "
                    + " where ua.username = '" + userName + "'");
            byte[] pass;
            TUserAccount result = null;
            if( rs.next() ){
                result = new TUserAccount(rs.getLong("recordindex"), userName, null, rs.getBoolean("isactive"));
                result.setGroup(new TUserGroup(rs.getLong("idgroup")));
                TUserContact uc = new TUserContact();
                uc.setFullName(rs.getString("fullname"));
                result.setUserContact(uc);
            }
            else{
                throw new SQLException("nUser Name tidak ditemukan");
            }
                
                rs = stm.executeQuery("select tp.* from TPemda tp where idpemda in "
                        + " (select idpemda from tuserpemda where iduser=" + result.getId() + ")");
                Pemda pemda = null;
                if(rs.next()){
                    pemda= new Pemda(rs.getLong("idpemda"), rs.getString("kodeprovinsi"), 
                            rs.getString("kodepemda"), rs.getString("kodesatker"), 
                            rs.getString("namapemda"), rs.getString("namapemdasingkat"), rs.getShort("tipepemda"));
                }
                result.setPemda(pemda);
                result.setMenus(getUserMenuStructures(result.getId(), loginConn));
            result.getGroup().setRights(getUserGroupMenuRight(result.getGroup().getId(), loginConn));
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal melakukan login pada aplikasi ini\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
       
    private List<TMenu> getUserMenuStructures(long idUser, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT tmenu.masterkey idmenu, useraccount, kode, menuname, mastermenu, menuitem, icon, "
                    + " tmm.masterkey mastermenuid, address, tmm.actives, parentmenu "
                    + "  FROM tmenu   "
                    + "  left join tmastermenu tmm on tmenu.mastermenu=tmm.masterkey "
                    + " left join tmenustructure tms on tmenu.masterkey=tms.submenu "
                    + "  where useraccount = " + idUser
                    + "  order by kode");
            List<TMenu> result = new ArrayList();
            TMenu m; 
            while( rs.next()){
                m = new TMenu(rs.getLong("idmenu"), rs.getString("kode"), rs.getString("menuname"), 
                        new MasterMenu(rs.getLong("mastermenuid"), rs.getString("address"), rs.getBoolean("actives")), 
                        rs.getBoolean("menuitem"), rs.getString("icon"));
                m.setParentMenu(new TMenu(rs.getLong("parentmenu")));
                result.add(m);
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil UserMenu saat melakukan login pada aplikasi ini\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    private List<TGroupRight> getUserGroupMenuRight(long idGrup, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT recordindex, masterkey mmkey , nama, address, actives, reads, writes "
                    + "  FROM tmastermenu "
                    + "  left join tgroupright on tmastermenu.masterkey=tgroupright.menu "
                    + "  where tgroupright.usergroup= " + idGrup);
            List<TGroupRight> result = new ArrayList();
            while( rs.next()){
                result.add(new TGroupRight(rs.getLong("recordindex"), new MasterMenu(rs.getLong("mmkey"), rs.getString("nama"), rs.getString("address"), rs.getBoolean("actives")), rs.getBoolean("reads"), rs.getBoolean("writes")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil GroupRight saat melakukan login pada aplikasi ini\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public boolean canRead(long idGrup, String menuName, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            String sql = "SELECT address, reads"
                    + "  FROM tmastermenu tmm "
                    + "  inner join tgroupright on tmm.masterkey=tgroupright.menu "
                    + "  where usergroup=" + idGrup + " and address = '" + menuName + "'";
            rs = stm.executeQuery(sql);
            
            if( rs.next()){
                return rs.getBoolean("reads");
            }
            return false;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil hak akses membaca\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public boolean canWrite(long idGrup, String menuName, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("SELECT address, writes"
                    + "  FROM tmastermenu tmm "
                    + "  inner join tgroupright on tmm.masterkey=tgroupright.menu "
                    + "  where usergroup=" + idGrup + " and address = '" + menuName + "'");
            
            if( rs.next()){
                return rs.getBoolean("writes");
            }
            return false;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil hak akses merubah data\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    public String[] getKodeSatkerKodePemda(String userName, String password, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            String[] result = null;
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select username, password, isactive, kodeprovinsi, kodepemda, kodesatker  "
                    + "from tuseraccount  "
                    + "left join tuserpemda on tuseraccount.recordindex=tuserpemda.iduser "
                    + "left join tpemda on tuserpemda.idpemda=tpemda.idpemda"
                    + " where tuseraccount.username = '" + userName + "'");
            byte[] pass;
            if( rs.next() ){
                pass = rs.getBytes("password");
                
                String pp = "";
                for (int i = 0; i < pass.length; i++) {
                    pp = pp+pass[i];
                }
                if (!pp.equals(password)) {
                    System.out.println("password dari agent : '"+password+"' \n password yang dibutuhkan '" + pp + "'");
                    throw new SQLException("Gagal melakukan transfer data\n Kata Sandi tidak Sesuai");
                }
                if( !rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE) ){
                    throw new SQLException("Gagal transfer data\n Akun anda tidak aktif, hubungi admin untuk mengaktifkan kembali akun anda");
                }
                result = new String[]{rs.getString("kodesatker"), rs.getString("kodeprovinsi")+"."+rs.getString("kodepemda")};
            }
            else{
                throw new SQLException("Gagal melakukan transfer data\nUser Name tidak ditemukan");
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal melakukan transfer data\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    
    
    //    @Override
//    public UserAccount login(String userName, byte[] encryptedPass, Connection loginConn, Connection sikdConn) throws SQLException{
//        Statement stm = null;
//        ResultSet rs = null;
//        try {
//            stm = loginConn.createStatement();
//            rs = stm.executeQuery("select ua.*, uc." + IMGRDBConstants.ATTR_FULL_NAME + " from "
//                    + IMGRDBConstants.TABLE_USER_ACCOUNT + " ua"
//                    + " inner join " +IMGRDBConstants.TABLE_USER_CONTACT + " uc"
//                    + " on ua."+IMGRDBConstants.ATTR_RECORD_INDEX + " = " + "uc." + IMGRDBConstants.ATTR_USER_ACCOUNT
//                    + " where ua." 
//                    + IMGRDBConstants.ATTR_USER_NAME + " = '" + userName + "'");
//            byte[] pass;
//            UserAccount result = null;
//            if( rs.next() ){
//                result = new UserAccount();
//                result.setIndex(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX));
//                result.setUserName(rs.getString(IMGRDBConstants.ATTR_USER_NAME));
//                pass = rs.getBytes(IMGRDBConstants.ATTR_PASSWORD);                
//                result.setPassword(Crypto.decrypt(pass));
//                result.setUserType(rs.getShort(IMGRDBConstants.ATTR_USER_TYPE));
//                result.setActive(rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE));
//                if (!Arrays.equals(pass, encryptedPass)) {
//                    throw new SQLException("Gagal melakukan login pada aplikasi ini\n Kata Sandi tidak Sesuai");
//                }
//                if( !rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE) ){
//                    throw new SQLException("Gagal melakukan login pada aplikasi ini\n Akun anda tidak aktif, hubungi admin untuk mengaktifkan kembali akun anda");
//                }
//                UserContact uc = new UserContact();
//                uc.setFullName(rs.getString(IMGRDBConstants.ATTR_FULL_NAME));
//                result.setUserContact(uc);
//            }
//            else{
//                throw new SQLException("Gagal melakukan login pada aplikasi ini\nUser Name tidak ditemukan");
//            }
//            
//            if (result.getUserType() == UserAccount.USER_TYPE_PEMDA_SHORT) {
//                rs = stm.executeQuery("select up.* from userpemda up where useraccount=" +result.getIndex());
//                UserPemda pemda = null;
//                if(rs.next()){
//                    pemda= new UserPemda();
//                    pemda.setIndex(rs.getLong("recordindex"));
//                    pemda.setPemdaType(rs.getShort("pemdatype"));
//                    pemda.setPemdaCode(rs.getString("pemdacode"));
//                    pemda.setPemdaName(rs.getString("pemdaname"));
//                    pemda.setSatkerCode(rs.getString("satkercode"));
//                }
//                result.setUserPemda(pemda);
//            }
//            result = getUserRefFromLogin(result, sikdConn);
//            long sessionid = System.currentTimeMillis();
//            createUserAuthentication(userName, String.valueOf(sessionid), sikdConn);
//            result.getGrup().setMenus(getMenuStructures(result.getGrup(), sikdConn));
//            return result;
//        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | InvalidKeySpecException | SQLException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
//            throw new SQLException("Gagal melakukan login pada aplikasi ini\n" + ex.getMessage());
//        }
//        finally{
//            if( rs != null ) rs.close();
//            if( stm != null ) stm.close();
//        }
//    }
    
    
    /*private UserAccount getUserRefFromLogin(UserAccount userAccount,  Connection sikdConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = sikdConn.createStatement();
            rs = stm.executeQuery("select * from userref where username='" + userAccount.getUserName().trim()+"'");
            UserRef ur = null;
            if( rs.next()){
                ur = new UserRef();
                ur.setIndex(rs.getLong("masterkey"));
                ur.setUserName(rs.getString("username"));
                ur.setUserType(rs.getShort("usertype"));
            }
            userAccount.setUserRef(ur);
            
            rs = stm.executeQuery("select ug.* from usergroup ug "
                    + "inner join userassociation ua on ug.masterkey=ua.usergroup "
                    + "inner join userref ur on ua.userref=ur.masterkey "
                    + "where ur.username='" + userAccount.getUserName().trim()+"'");
            List<UserGroup> grups = new ArrayList();
            if(rs.next()){
                UserGroup ug = new UserGroup(rs.getLong("masterkey"), rs.getString("groupname"), rs.getString("description"));
                ug.setGroupType(rs.getShort("grouptype"));
                userAccount.setGrup(ug);
//                grups.add();
            }
            
            
            return userAccount;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil UserGrup saat melakukan login pada aplikasi ini\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    private void createUserAuthentication(String userName, String sessionid, Connection conn) throws SQLException{
        Statement stm = null;
        String sql = "insert into " + IMGRDBConstants.TABLE_USER_AUTHENTICATION
                    + " VALUES ('" + userName + "', '" + sessionid + "')";
        try {
            stm = conn.createStatement();
            stm.executeUpdate(sql );
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    private List<AppMenu> getMenuStructures(UserGroup usergrup, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        String sql = "select m.*, mr.* from appmenu m "
                + "inner join appmenuright mr on m.masterkey=mr.appmenu "
                + "where usergroup=" + usergrup.getMasterkey() + " and  "
                + "masterkey not in(select parentmenu from appmenustructure)";
        System.out.println("sql " + sql);
        try {
            stm = conn.createStatement();
                rs = stm.executeQuery(sql);
            
            
            List<AppMenu> result = new ArrayList();
            while( rs.next() ){
                AppMenu ap = new AppMenu(rs.getLong(IMGRDBConstants.ATTR_MASTERKEY), 
                        rs.getString(IMGRDBConstants.ATTR_MENU_NAME), 
                        rs.getBoolean(IMGRDBConstants.ATTR_IS_MENU_ITEM),rs.getBoolean("isread"), rs.getBoolean("iswrite"));
                result.add(ap);
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    /*
    public boolean canEnter(String userName, byte[] encryptedPass, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select ua.* from "
                    + IMGRDBConstants.TABLE_USER_ACCOUNT + " ua "
                    + " where ua." 
                    + IMGRDBConstants.ATTR_USER_NAME + " = '" + userName + "'");
            byte[] pass;
            boolean result;
            if( rs.next() ){
                pass = rs.getBytes(IMGRDBConstants.ATTR_PASSWORD);  
                byte[] epass = Crypto.encrypt(encryptedPass);
                if (!Arrays.equals(pass, epass)) {
                    throw new SQLException("Password yang anda masukan tidak sesuai");
                }
                else result = true;
            }
            else{
                throw new SQLException("User name "+userName+ " tidak ditemukan");
            }
            
            
            return result;

        } catch (SQLException ex) {
            throw new SQLException("Gagal mencari user account yang sesuai\n" + ex.getMessage());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            Logger.getLogger(LoginSQL.class.getName()).log(Level.SEVERE, null, ex);
            throw new SQLException("Gagal mencari user account yang sesuai\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void deleteUserAccount(String userName, Connection loginConn) throws SQLException{
        Statement stm = null;
        try {
            stm = loginConn.createStatement();
            stm.executeUpdate("delete from  "
                    + IMGRDBConstants.TABLE_USER_ACCOUNT 
                    + " where " 
                    + IMGRDBConstants.ATTR_USER_NAME + " = '" + userName + "'");
        } catch (SQLException ex) {
            throw new SQLException("Gagal menghapus data pengguna \n" + ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    @Override
    public List<UserPemda> getPemdas(Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT *  FROM userpemda order by pemdacode");
            List<UserPemda> result = new ArrayList<>();
            while(rs.next()){
                result.add(new UserPemda(rs.getLong("recordindex"), rs.getShort("pemdatype"), rs.getString("pemdacode"), rs.getString("pemdaname"), rs.getString("satkercode")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil seluruh Pemda");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
        
    }
    
    @Override
    public List<UserPemda> getPemdaKompilasis(Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null ;
        try {
            stm = conn.createStatement();
            rs = stm.executeQuery("SELECT *  FROM userpemda order by pemdacode");
            List<UserPemda> result = new ArrayList<>();
            result.add(new UserPemda(0,(short)0,"00.00", "Nasional", "000000"));
            while(rs.next()){
                short pemdatipe = rs.getShort("pemdatype");
                String pemdaC = rs.getString("pemdacode");
                String pemdaN = rs.getString("pemdaname");
                String satkerC = rs.getString("satkercode");
                result.add(new UserPemda(rs.getLong("recordindex"), pemdatipe, pemdaC, pemdaN, satkerC));
                if( pemdatipe == UserPemda.PEMDA_TYPE_PROVINSI_SHORT ){
                    result.add(new UserPemda(0,(short)0,pemdaC.trim()+"0_", "Kompilasi Kab/Kota "+pemdaN, satkerC+"0_"));
                    result.add(new UserPemda(0,(short)0,pemdaC.trim()+"1_", "Kompilasi Seluruh "+pemdaN, satkerC+"1_"));
                }
            }
            
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil seluruh Pemda");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
        
    }
        
    @Override
    public List<UserPemda> getPemdas(String pemdacode, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null ;
        try {
            stm = conn.createStatement();
//            rs = stm.executeQuery("SELECT distinct pemdacode, pemdaname, satkercode  FROM userpemda order by pemdacode");
            rs = stm.executeQuery("SELECT *  FROM userpemda " +
                                  "where pemdacode like '" + pemdacode.trim().substring(0,3) + "%' " +
                                  "order by pemdacode");
            List<UserPemda> result = new ArrayList<>();
            while(rs.next()){
                result.add(new UserPemda(rs.getLong("recordindex"), rs.getShort("pemdatype"), rs.getString("pemdacode"), rs.getString("pemdaname"), rs.getString("satkercode")));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil seluruh Pemda");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
        
    }
    
    @Override
    public List<String> getPemdaCodeUnders(String satkercode, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null ;
        try {
            stm = conn.createStatement();
            String satker = "", endw="";
            if( satkercode != null && !satkercode.trim().equals("")){
                if( satkercode.trim().endsWith("_") ){
                    endw = satkercode.trim().substring(satkercode.trim().length()-2, satkercode.trim().length());
                    satker = satkercode.trim().substring(0, satkercode.trim().length()-2);
                }
                else satker = satkercode;
            }
            String sql = "";
            if( endw.trim().startsWith("0") )
                sql = "select * from userpemda where pemdacode <> (select pemdacode from userpemda where satkercode='"+satker+"') " +
"and pemdacode like (select substring( " +
"(select pemdacode from userpemda where satkercode='"+satker+"') from 1 for 3) || '%'" +
")" +
" order by pemdacode";
            else if( endw.trim().startsWith("1") )
                sql = "select * from userpemda where pemdacode like (select substring( " +
"(select pemdacode from userpemda where satkercode='"+satker+"') from 1 for 3) || '%'" +
")" +
" order by pemdacode";
            
            rs = stm.executeQuery(sql);
            List<String> result = new ArrayList<>();
            while(rs.next()){
                result.add(rs.getString("pemdacode"));
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal mengambil seluruh Kode Pemda");
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
        
    }
    
    
    @Override
    public String getKodeSatker(String userName, String password, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select ua.*, up.satkercode from "
                    + IMGRDBConstants.TABLE_USER_ACCOUNT + " ua"
                    + " inner join " +IMGRDBConstants.TABLE_USER_PEMDA + " up"
                    + " on ua."+IMGRDBConstants.ATTR_RECORD_INDEX + " = " + "up." + IMGRDBConstants.ATTR_USER_ACCOUNT
                    + " where ua." 
                    + IMGRDBConstants.ATTR_USER_NAME + " = '" + userName + "'");
            byte[] pass;
            String result = null;
            if( rs.next() ){
                pass = rs.getBytes(IMGRDBConstants.ATTR_PASSWORD);
                
                String pp = "";
                for (int i = 0; i < pass.length; i++) {
                    pp = pp+pass[i];
                }
//                if (!Arrays.equals(pass, password)) {
//                    throw new SQLException("Gagal melakukan transfer data\n Kata Sandi tidak Sesuai");
//                }
                if (!pp.equals(password)) {
                    System.out.println("password dari agent : '"+password+"' \n password yang dibutuhkan '" + pp + "'");
                    throw new SQLException("Gagal melakukan transfer data\n Kata Sandi tidak Sesuai");
                }
                if( !rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE) ){
                    throw new SQLException("Gagal transfer data\n Akun anda tidak aktif, hubungi admin untuk mengaktifkan kembali akun anda");
                }
                result = rs.getString("satkercode");
            }
            else{
                throw new SQLException("Gagal melakukan transfer data\nUser Name tidak ditemukan");
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal melakukan transfer data\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    
    @Override
    public String getKodePemda(String kodeSatker, Connection loginConn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = loginConn.createStatement();
            rs = stm.executeQuery("select up.pemdacode from "
                    + IMGRDBConstants.TABLE_USER_PEMDA + " up"
                    + " where satkercode = '"+ kodeSatker + "'");
            String result = null;
            if( rs.next() ){
                result = rs.getString("pemdacode");
            }
            return result;
        } catch (SQLException ex) {
            throw new SQLException("Gagal melakukan transfer data\n" + ex.getMessage());
        }
        finally{
            if( rs != null ) rs.close();
            if( stm != null ) stm.close();
        }
    }
    
    
    @Override
    public List<UserAccount> getUserAccounts(Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            List<UserAccount> result = new ArrayList();
            stm = conn.createStatement();
            rs = stm.executeQuery("Select * from UserAccount");
            while (rs.next()) {
                UserAccount uc = new UserAccount(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getString(IMGRDBConstants.ATTR_USER_NAME), rs.getBytes(IMGRDBConstants.ATTR_PASSWORD), rs.getShort(IMGRDBConstants.ATTR_USER_TYPE), rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE));
                uc.setPassword(Crypto.decrypt(uc.getPassword()));
                result.add(uc);
            }
            return result;
        } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!=null ) rs.close();
            if(stm!=null) stm.close();
        }
    }
    
    
    public UserAccount getUserAccount(String kodeSatker, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            UserAccount ua = new UserAccount();
            stm = conn.createStatement();
            rs = stm.executeQuery("Select ua.* from UserAccount ua inner join userpemda up on ua.recordindex=up.useraccount where up.satkercode='"+kodeSatker+"'");
            if (rs.next()) {
                ua = new UserAccount(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getString(IMGRDBConstants.ATTR_USER_NAME), rs.getBytes(IMGRDBConstants.ATTR_PASSWORD), rs.getShort(IMGRDBConstants.ATTR_USER_TYPE), rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE));
                ua.setPassword(Crypto.decrypt(ua.getPassword())); 
                
                UserContact uc = getUserContact(ua.getIndex(), conn);
                UserPemda up = getUserPemda(ua.getIndex(), conn);
                ua.setUserContact(uc);
                if(up==null) up = new UserPemda();
                ua.setUserPemda(up);
            }
            return ua;
        } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!=null ) rs.close();
            if(stm!=null) stm.close();
        }
    }
    
    public UserAccount getUserAccountByName(String userName, Connection conn) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;
        try {
            UserAccount ua = new UserAccount();
            stm = conn.createStatement();
            rs = stm.executeQuery("Select * from UserAccount where " + IMGRDBConstants.ATTR_USER_NAME + "='"+userName+"'");
            if (rs.next()) {
                ua = new UserAccount(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getString(IMGRDBConstants.ATTR_USER_NAME), rs.getBytes(IMGRDBConstants.ATTR_PASSWORD), rs.getShort(IMGRDBConstants.ATTR_USER_TYPE), rs.getBoolean(IMGRDBConstants.ATTR_IS_ACTIVE));
                ua.setPassword(Crypto.decrypt(ua.getPassword())); 
                
                UserContact uc = getUserContact(ua.getIndex(), conn);
                UserPemda up = getUserPemda(ua.getIndex(), conn);
                ua.setUserContact(uc);
                if(up==null) up = new UserPemda();
                ua.setUserPemda(up);
            }
            return ua;
        } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!=null ) rs.close();
            if(stm!=null) stm.close();
        }
    }
    
    @Override
    public void updateUserAccount(UserAccount userAccount, Connection conn) throws SQLException{
        PreparedStatement stm = null; 
        String sql = "update " + IMGRDBConstants.TABLE_USER_ACCOUNT 
                    + " set " + IMGRDBConstants.ATTR_USER_NAME + " ='" + userAccount.getUserName() + "', "
                    + IMGRDBConstants.ATTR_PASSWORD + " = ?,  " 
                    + IMGRDBConstants.ATTR_USER_TYPE + " = ?, "
                    + IMGRDBConstants.ATTR_IS_ACTIVE + " = ? where "
                    + IMGRDBConstants.ATTR_RECORD_INDEX + " = ? ";
        try {
            stm = conn.prepareStatement( sql );
            stm.setBytes(1, Crypto.encrypt(userAccount.getPassword()));
            stm.setShort(2, userAccount.getUserType());
            stm.setBoolean(3, userAccount.isActive());
            stm.setLong(4, userAccount.getIndex());
            stm.executeUpdate();
        } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if(stm!=null) stm.close();
        }
    }
    
    @Override
    public long createUserAccount(UserAccount user, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        String sql = "insert into " + IMGRDBConstants.TABLE_USER_ACCOUNT 
                    + " (username, password, usertype, isactive) values (?, ?, ?, ?)";
        try{
            stm = conn.prepareStatement(sql);
            stm.setString(1, user.getUserName());
            stm.setBytes(2, Crypto.encrypt(user.getPassword()));
            stm.setShort(3, user.getUserType());
            stm.setBoolean(4, user.isActive());
            stm.executeUpdate();
            return getMaxIndex(IMGRDBConstants.TABLE_USER_ACCOUNT, IMGRDBConstants.ATTR_RECORD_INDEX, conn);
        }
        catch(SQLException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex){
         throw new SQLException(ex.getMessage());   
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    
    @Override
    public UserContact getUserContact(long idUserAccount, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();            
            rs = stm.executeQuery("select * from " + IMGRDBConstants.TABLE_USER_CONTACT 
                    + " where " + IMGRDBConstants.ATTR_USER_ACCOUNT + " = "  + idUserAccount );
            UserContact result = null;
            if(rs.next()) {
                result = new UserContact(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getString(IMGRDBConstants.ATTR_FULL_NAME), rs.getString(IMGRDBConstants.ATTR_PHONE), rs.getString(IMGRDBConstants.ATTR_EMAIL), rs.getString(IMGRDBConstants.ATTR_WORKPLACE),rs.getString(IMGRDBConstants.ATTR_ADDRESS));
            }
            return result;
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!= null ) rs.close();
            if( stm!=null ) stm.close();
        }
    }    
    
    @Override
    public void updateUserContact(UserContact userContact, Connection conn ) throws SQLException{
        PreparedStatement stm = null;
        try {
            stm = conn.prepareStatement("update "+ IMGRDBConstants.TABLE_USER_CONTACT + " set "
                    + IMGRDBConstants.ATTR_FULL_NAME + " = ?, "
                    + IMGRDBConstants.ATTR_PHONE + " = ?, "
                    + IMGRDBConstants.ATTR_EMAIL + " = ?, "
                    + IMGRDBConstants.ATTR_WORKPLACE + " = ?, "
                    + IMGRDBConstants.ATTR_ADDRESS + " = ?"
                    + " where " + IMGRDBConstants.ATTR_RECORD_INDEX + " = ?");
            stm.setString(1, userContact.getFullName());
            stm.setString(2, userContact.getPhone());
            stm.setString(3, userContact.getEmail());
            stm.setString(4, userContact.getWorkPlace());
            stm.setString(5, userContact.getAddress()); 
            stm.setLong(6, userContact.getIndex());
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    @Override
    public long createUserContact(UserContact obj, long idUserAccount, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        try{
            stm = conn.prepareStatement("insert into " + IMGRDBConstants.TABLE_USER_CONTACT 
                    + " (useraccount, fullname, phone, email, workplace, address) values (?, ?, ?, ?, ?, ?)");
            stm.setLong(1, idUserAccount);
            stm.setString(2, obj.getFullName());
            stm.setString(3, obj.getPhone());
            stm.setString(4, obj.getEmail());
            stm.setString(5, obj.getWorkPlace());
            stm.setString(6, obj.getAddress());
            stm.executeUpdate();
            return getMaxIndex(IMGRDBConstants.TABLE_USER_CONTACT, IMGRDBConstants.ATTR_RECORD_INDEX, conn);
        }
        catch(SQLException ex){
         throw new SQLException(ex.getMessage());   
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    @Override
    public long createUserPemda(UserPemda obj, long idUserAccount, Connection conn) throws SQLException{
        PreparedStatement stm = null;
        try{
            stm = conn.prepareStatement("insert into " + IMGRDBConstants.TABLE_USER_PEMDA 
                    + " (useraccount, pemdatype, pemdacode, pemdaname, satkercode) values (?, ?, ?, ?, ?)");
            stm.setLong(1, idUserAccount);
            stm.setShort(2, obj.getPemdaType());
            stm.setString(3, obj.getPemdaCode());
            stm.setString(4, obj.getPemdaName());
            stm.setString(5, obj.getSatkerCode());
            stm.executeUpdate();
            return getMaxIndex(IMGRDBConstants.TABLE_USER_PEMDA, IMGRDBConstants.ATTR_RECORD_INDEX, conn);
        }
        catch(SQLException ex){
         throw new SQLException(ex.getMessage());   
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    
    @Override
    public UserPemda getUserPemda(long idUserAccount, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();            
            rs = stm.executeQuery("select * from " + IMGRDBConstants.TABLE_USER_PEMDA 
                    + " where " + IMGRDBConstants.ATTR_USER_ACCOUNT + " = "  + idUserAccount );
            UserPemda result = null;
            if(rs.next()) {
                result = new UserPemda(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getShort(IMGRDBConstants.ATTR_PEMDA_TYPE), rs.getString(IMGRDBConstants.ATTR_PEMDA_CODE), rs.getString(IMGRDBConstants.ATTR_PEMDA_NAME), rs.getString(IMGRDBConstants.ATTR_SATKER_CODE));
            }
            return result;
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!= null ) rs.close();
            if( stm!=null ) stm.close();
        }
    }    
    
    @Override
    public UserPemda getUserPemda(String kodeSatker, Connection conn) throws SQLException{
        Statement stm = null;
        ResultSet rs = null;
        try {
            stm = conn.createStatement();            
            rs = stm.executeQuery("select * from " + IMGRDBConstants.TABLE_USER_PEMDA 
                    + " where " + IMGRDBConstants.ATTR_SATKER_CODE + " = '"  + kodeSatker + "'" );
            UserPemda result = null;
            if(rs.next()) {
                result = new UserPemda(rs.getLong(IMGRDBConstants.ATTR_RECORD_INDEX), rs.getShort(IMGRDBConstants.ATTR_PEMDA_TYPE), rs.getString(IMGRDBConstants.ATTR_PEMDA_CODE), rs.getString(IMGRDBConstants.ATTR_PEMDA_NAME), rs.getString(IMGRDBConstants.ATTR_SATKER_CODE));
            }
            return result;
            
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( rs!= null ) rs.close();
            if( stm!=null ) stm.close();
        }
    }    
    
    @Override
    @SuppressWarnings("ConvertToTryWithResources")
    public void updateUserPemda(UserPemda userPemda, Connection conn ) throws SQLException{
        PreparedStatement stm = null;
        try {
            stm = conn.prepareStatement("update "+ IMGRDBConstants.TABLE_USER_PEMDA + " set "
                    + IMGRDBConstants.ATTR_PEMDA_TYPE + " = ?, "
                    + IMGRDBConstants.ATTR_PEMDA_CODE + " = ?, "
                    + IMGRDBConstants.ATTR_PEMDA_NAME + " = ?, "
                    + IMGRDBConstants.ATTR_SATKER_CODE + " = ? "
                    + " where " + IMGRDBConstants.ATTR_RECORD_INDEX + " = ?");
            stm.setShort(1, userPemda.getPemdaType());
            stm.setString(2, userPemda.getPemdaCode());
            stm.setString(3, userPemda.getPemdaName());
            stm.setString(4, userPemda.getSatkerCode());
            stm.setLong(5, userPemda.getIndex());
            
            stm.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        finally{
            if( stm != null ) stm.close();
        }
    }
    */
}