package app.sikd.login.ejb.db;

/**
 *
 * @author detra
 */
import app.sikd.util.PropertiesLoader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author detra
 */
public class LoginConnectionManager implements Serializable{
    String uploadPath;
    public Connection createConnection(String fileName) throws Exception{        
        try {
            Properties pr = PropertiesLoader.loadProperties(fileName);
            Class.forName("org.postgresql.Driver");
            String dbServer = pr.getProperty("dbServer");
            String dbName = pr.getProperty("dbName");
            String dbPort = pr.getProperty("dbPort");
            uploadPath = pr.getProperty("uploadPath");
            return DriverManager.getConnection("jdbc:postgresql://" 
                    + dbServer + ":" + dbPort + "/" + dbName, 
                    "postgres", "postgres");
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(LoginConnectionManager.class.getName()).log(Level.SEVERE, "", ex.getMessage());
            throw new Exception(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(LoginConnectionManager.class.getName()).log(Level.SEVERE, "", ex.getMessage());
            throw new Exception(ex.getMessage());
//            Logger.getLogger(SIKDConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
}