/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.login.ejb.db;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import app.sikd.entity.mgr.TGroupRight;
import app.sikd.entity.mgr.TUserGroup;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author sora
 */
public interface IAdministrasiPengguna {
    
    public List<TUserGroup> getSubUserGroups(TUserGroup parentGroup, Connection loginConn) throws SQLException;
    public TUserGroup getUserGroup(long idGroup, Connection loginConn) throws SQLException;
    public TUserGroup createUserGroup(TUserGroup grup, Connection loginConn) throws SQLException;
    public void deleteUserGroup(TUserGroup grup, Connection loginConn) throws SQLException;
    public void updateUserGroup(TUserGroup grup, Connection loginConn) throws SQLException;
    
    public WilayahKerja getWilayahKerja(long idWK, Connection loginConn) throws SQLException;
    public  List<WilayahKerja> getWilayahKerjas(Connection loginConn) throws SQLException;
    public WilayahKerja createWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException;
    public void deleteWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException;
    public void updateWilayahKerja(WilayahKerja obj, Connection loginConn) throws SQLException;
    
    public Pemda getPemda(long idPemda, Connection loginConn) throws SQLException;
    public  List<Pemda> getPemdas(Connection loginConn) throws SQLException;
    public Pemda createPemda(Pemda obj, Connection loginConn) throws SQLException;
    public void deletePemda(Pemda obj, Connection loginConn) throws SQLException;
    public void updatePemda(Pemda obj, Connection loginConn) throws SQLException;
    public List<Pemda> getProvinsis(Connection loginConn) throws SQLException;
    public List<Pemda> getPemdaByProvinsi(Pemda prov, Connection loginConn) throws SQLException;
    
    public List<TGroupRight> getGroupRight(long idGrup, Connection loginConn) throws SQLException;
    public List<TGroupRight> createGroupRights(long groupId, List<TGroupRight> objects, Connection loginConn) throws SQLException;
    public void deleteGroupRightByGroup(long groupId, Connection loginConn) throws SQLException;
    public void updateGroupRight(long groupId, List<TGroupRight> objects, Connection loginConn) throws SQLException;
    
}
