package app.sikd.login.ejb.db;

import app.sikd.entity.mgr.TUserAccount;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author detra
 */
public interface ILoginSQL {
//    public UserAccount login(String userName, byte[] encryptedPass, Connection loginConn, Connection sikdConn) throws SQLException;
    public TUserAccount login(String userName, byte[] encryptedPass, Connection loginConn) throws SQLException;
    public TUserAccount getUserbyName(String userName, Connection loginConn) throws SQLException;
    public boolean canRead(long idGrup, String menuName, Connection loginConn) throws SQLException;
    public boolean canWrite(long idGrup, String menuName, Connection loginConn) throws SQLException;
    
    public String[] getKodeSatkerKodePemda(String userName, String password, Connection loginConn) throws SQLException;
    
    /*public boolean canEnter(String userName, byte[] encryptedPass, Connection loginConn) throws SQLException;    
    public List<UserPemda> getPemdas(Connection conn) throws SQLException;
    public List<UserPemda> getPemdas(String pemdacode, Connection conn) throws SQLException;
    public List<String> getPemdaCodeUnders(String satkercode, Connection conn) throws SQLException;
    public List<UserPemda> getPemdaKompilasis(Connection conn) throws SQLException;
    public String getKodeSatker(String userName, String password, Connection loginConn) throws SQLException;
    public String getKodePemda(String kodeSatker, Connection loginConn) throws SQLException;
    
    public void deleteUserAccount(String userName, Connection loginConn) throws SQLException;
    public UserAccount getUserAccountByName(String userName, Connection conn) throws SQLException;
    public List<UserAccount> getUserAccounts(Connection conn) throws SQLException;
    public UserAccount getUserAccount(String kodeSatker, Connection conn) throws SQLException;
    public void updateUserAccount(UserAccount userAccount, Connection conn) throws SQLException;
    public long createUserAccount(UserAccount user, Connection conn) throws SQLException;
    public UserContact getUserContact(long idUserAccount, Connection conn) throws SQLException;
    public void updateUserContact(UserContact userContact, Connection conn ) throws SQLException;
    public long createUserContact(UserContact obj, long idUserAccount, Connection conn) throws SQLException;
    public long createUserPemda(UserPemda obj, long idUserAccount, Connection conn) throws SQLException;
    public UserPemda getUserPemda(long idUserAccount, Connection conn) throws SQLException;
    public UserPemda getUserPemda(String kodeSatker, Connection conn) throws SQLException;
    public void updateUserPemda(UserPemda userPemda, Connection conn ) throws SQLException;*/
}
